var initial = 0;

var display = document.getElementById("value");

display.innerHTML = initial;

function displayNumMinus() {
    display.innerHTML = --initial;
}

function displayNumReset() {
    display.innerHTML = 0;
    initial = 0;
}

function displayNumPlus() {
    display.innerHTML = ++initial;
}
